package id.co.garudaindonesia.gaaccess_play;

public interface CourierInterface {
    String getId();
    String getName();
    String getPhoneNumber();
}

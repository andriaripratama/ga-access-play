package id.co.garudaindonesia.gaaccess_play.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.garudaindonesia.gaaccess_play.model.ApplicationUser;
import id.co.garudaindonesia.gaaccess_play.repo.ApplicationUserRepo;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static id.co.garudaindonesia.gaaccess_play.auth.SecurityConstant.*;

public class JWTAuthenFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private ApplicationUserRepo applicationUserRepo;
    private String email;
    private ApplicationUser user;

    public JWTAuthenFilter(AuthenticationManager authenticationManager, ApplicationUserRepo applicationUserRepo) {
        this.authenticationManager = authenticationManager;
        this.applicationUserRepo = applicationUserRepo;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        try {
            ApplicationUser creds = new ObjectMapper().readValue(request.getInputStream(), ApplicationUser.class);

            if (creds.getEmail() == null) {
                user = applicationUserRepo.findByCellNumber(creds.getCellNumber());
                email = user.getEmail();
            } else {
                email = creds.getEmail();
            }

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    email,
                    creds.getPassword(),
                    new ArrayList<>()
            ));


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
        System.out.println("failed");

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

        ApplicationUser user = applicationUserRepo.findByEmail(((User) authResult.getPrincipal()).getUsername());
        Integer isActive = user.getIsActive();

        if (isActive.equals(0)) {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.getWriter().write("{\"message\":\"User not active\"}");
        } else {
            //Date expiresAt = new Date(System.currentTimeMillis() + EXPIRATION_TIME_1HOUR);
//            String token = JWT.create()
//                    .withSubject(((User) authResult.getPrincipal()).getUsername())
//                    .withExpiresAt(expiresAt)
//                    .sign(Algorithm.HMAC512(SECRET.getBytes()));

            String token = JWT.create()
                    .withSubject(((User) authResult.getPrincipal()).getUsername())
                    .sign(Algorithm.HMAC512(SECRET.getBytes()));

            response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
//            response.addHeader(HEADER_GA_SESSION_TIME, expiresAt.toString());

            String message = "{" +
                    "\"message\":\"success\"," +
                    "\"user_id\":"+ user.getId() +"," +
                    "\"user_code\":\""+ user.getCode() +"\"," +
                    "\"user_address_latitude\":\""+ user.getAddressLat() +"\"," +
                    "\"user_address_longitude\":\""+ user.getAddressLong() +"\"," +
                    "\"user_token\":\""+ token +"\"" +
                    "}";

//            "\"user_token_expires\":\""+ expiresAt.toString() +"\"" +

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(message);

        }

    }
}

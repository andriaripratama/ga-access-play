package id.co.garudaindonesia.gaaccess_play.auth;

public class SecurityConstant {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final long EXPIRATION_TIME_1HOUR = 3600000; // 1 hour
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String HEADER_GA_SESSION_TIME = "GA-Session-Expire";
    public static final String SIGN_UP_URL = "/auth/register";
    public static final String PAYMENT_NOTIFY = "/api/payment/notify";
    public static final String PARTNER_API = "/api/partner/**";
}

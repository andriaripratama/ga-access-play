package id.co.garudaindonesia.gaaccess_play.service;

import id.co.garudaindonesia.gaaccess_play.model.ApplicationUser;
import id.co.garudaindonesia.gaaccess_play.repo.ApplicationUserRepo;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private ApplicationUserRepo applicationUserRepo;

    public UserDetailsServiceImpl(ApplicationUserRepo applicationUserRepo) {
        this.applicationUserRepo = applicationUserRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        ApplicationUser applicationUser = applicationUserRepo.findByEmail(s);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(s);
        }

        return new User(applicationUser.getEmail(), applicationUser.getPassword(), emptyList());

    }
}

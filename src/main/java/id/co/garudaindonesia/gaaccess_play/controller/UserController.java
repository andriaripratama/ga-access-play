package id.co.garudaindonesia.gaaccess_play.controller;

import id.co.garudaindonesia.gaaccess_play.helper.RandomString;
import id.co.garudaindonesia.gaaccess_play.model.ApplicationUser;
import id.co.garudaindonesia.gaaccess_play.repo.ApplicationUserRepo;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;


import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

@RestController
@RequestMapping("/auth")
public class UserController {

    private ApplicationUserRepo applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(ApplicationUserRepo applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    private void sendEmail(String recipient, String recipientAddress, String token) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("andriaripratama@gmail.com", "andri@google1");
            }
        });

        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("no-reply-access@garuda-indonesia.com", true));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientAddress));
        msg.setSubject("ACCESS - Account Verification");
        String messageContent = "Hi " + recipient + "!<br> Thank you for choosing Access! Before you begin using our services, kindly to verify your account by click this <a href=\"https://account.access.id/verify/" + token + "\">link</a>.<br><br>Thank you!<br>Access Team";
        msg.setContent(messageContent, "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(messageContent, "text/html");

        //Multipart multipart = new MimeMultipart();
        //multipart.addBodyPart(messageBodyPart);
        //MimeBodyPart attachPart = new MimeBodyPart();

        //attachPart.attachFile("/var/tmp/image19.png");
        //multipart.addBodyPart(attachPart);
        //msg.setContent(multipart);

        Transport.send(msg);
    }

    //ResponseEntity<Void>
    //ResponseEntity<String>
    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public HashMap<String,String> signUp(@RequestBody ApplicationUser user, HttpServletResponse response) {
        HashMap<String, String> messages = new HashMap<String,String>();

        if (user.getAddress() == null || user.getCellNumber() == null) {
            messages.put("message","Please fill all form");
            return messages;
        }

        if (applicationUserRepository.findByEmail(user.getEmail()) != null || applicationUserRepository.findByCellNumber(user.getCellNumber()) != null) {
            messages.put("message","User already exist!");

            //return new ResponseEntity<>(, HttpStatus.OK);
            return messages;
        } else {

            RandomString token = new RandomString();
            String tokenString = token.nextString();

            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setIsActive(0);
            user.setActivationToken(tokenString);

            applicationUserRepository.save(user);

            try {
                sendEmail(user.getFullName(), user.getEmail(), tokenString);
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            messages.put("message","User created!");

            //return new ResponseEntity<>("User already registered!", HttpStatus.OK);
            return messages;
        }
    }

    @GetMapping("/profile")
    public Authentication myProfile() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}

package id.co.garudaindonesia.gaaccess_play.controller;

import com.google.gson.Gson;
import id.co.garudaindonesia.gaaccess_play.model.*;
import id.co.garudaindonesia.gaaccess_play.repo.*;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/partner")
public class PartnerController {

//    This API for our partner.
//    Not for internal usage.

    private TempReservationHeaderRepo tempReservationHeaderRepo;
    private TempReservationDetailRepo tempReservationDetailRepo;
    private ReservationStatusRepo reservationStatusRepo;
    private PartnerRepo partnerRepo;
    private PickupRepo pickupRepo;
    private Gson gson;
    private JSONObject returnMessage;

    public PartnerController(TempReservationHeaderRepo tempReservationHeaderRepo, TempReservationDetailRepo tempReservationDetailRepo, ReservationStatusRepo reservationStatusRepo, PartnerRepo partnerRepo, PickupRepo pickupRepo, Gson gson) {
        this.tempReservationHeaderRepo = tempReservationHeaderRepo;
        this.tempReservationDetailRepo = tempReservationDetailRepo;
        this.reservationStatusRepo = reservationStatusRepo;
        this.partnerRepo = partnerRepo;
        this.pickupRepo = pickupRepo;
        this.gson = gson;
    }

    @GetMapping(value = "/reservations", produces = "application/json")
    private List<TempReservationHeader> getPartnerReservations(@RequestHeader(value = "TAUBERES-APPKEY") String appKey, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");
        Partner partner = partnerRepo.findByPartnerKey(appKey);

        if (partner != null) {
            List<TempReservationHeader> reservationHeader = tempReservationHeaderRepo.findByVendorCode(partner.getVendorCode());

            if (reservationHeader != null) {
                response.setStatus(HttpServletResponse.SC_ACCEPTED);

                return reservationHeader;
            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);

                return null;
            }

        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

            return null;
        }
    }

    @GetMapping(value = "/reservation/{bookingNo}", produces = "application/json")
    private String getPartnerReservation(@RequestHeader(value = "TAUBERES-APPKEY") String appKey, @PathVariable String bookingNo, HttpServletResponse response) throws IOException {
        Partner partner = partnerRepo.findByPartnerKey(appKey);
        returnMessage = new JSONObject();

        if (partner != null) {
            TempReservationHeader reservationHeader = tempReservationHeaderRepo.findReservationForPartner(partner.getVendorCode(), bookingNo);

            if (reservationHeader != null) {
                List<TempReservationDetail> reservationDetails = tempReservationDetailRepo.findByTempBookingNo(bookingNo);

                returnMessage.put("header", new JSONObject(reservationHeader));
                returnMessage.put("details", reservationDetails);

                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                //response.getWriter().write(returnMessage.toString());
                return returnMessage.toString();

            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);

                return null;
            }

        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

            return null;
        }
    }

    @GetMapping(value = "/pickup", produces = "application/json")
    private void getPickupDrivers(@RequestHeader(value = "TAUBERES-APPKEY") String appKey, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");
        Partner partner = partnerRepo.findByPartnerKey(appKey);

        if (partner != null) {
            List<Pickup> pickups = pickupRepo.findByVendorCode(partner.getVendorCode());

            if (pickups != null) {
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                response.getWriter().write(gson.toJson(pickups));
            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                response.getWriter().write("");
            }

        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().write("");
        }

    }

    @PostMapping(value = "/pickup", produces = "application/json")
    private void savePickupDriver(@RequestHeader(value = "TAUBERES-APPKEY") String appKey, @RequestBody String body, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");
        Partner partner = partnerRepo.findByPartnerKey(appKey);
        returnMessage = new JSONObject();

        if (partner != null) {

            JSONObject param = new JSONObject(body);

            Pickup pickup = new Pickup();
            pickup.setVendorCode(partner.getVendorCode());
            pickup.setBookingNo(param.getString("bookingNo"));
            pickup.setDriverId(param.getJSONObject("driver").getString("id"));
            pickup.setDriverName(param.getJSONObject("driver").getString("fullname"));
            pickup.setVehicleLicense(param.getJSONObject("driver").getString("vehicleNo"));
            pickup.setDriverStatus("ASSIGNED");

            pickupRepo.save(pickup);

            returnMessage.put("id", pickup.getId());
            returnMessage.put("message", "Success");

            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(returnMessage.toString());

        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().write("");
        }

    }

    @PostMapping(value = "/shipment_status")
    private void updateStatus(@RequestHeader(value = "TAUBERES-APPKEY") String appKey, @RequestBody String body, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");
        Partner partner = partnerRepo.findByPartnerKey(appKey);
        returnMessage = new JSONObject();

        if (partner != null) {

            JSONObject param = new JSONObject(body);

            reservationStatusRepo.updateShipmentStatus(param.getString("statusCode"), param.getString("bookingNo"));

            ReservationStatus status = new ReservationStatus();
            status.setBookingNo(param.getString("bookingNo"));
            status.setBookingStatus(param.getString("statusCode"));
            reservationStatusRepo.save(status);

            returnMessage.put("status", "Success");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(returnMessage.toString());

        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().write("");
        }
    }
}

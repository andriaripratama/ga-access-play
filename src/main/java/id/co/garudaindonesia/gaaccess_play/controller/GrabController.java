package id.co.garudaindonesia.gaaccess_play.controller;

import id.co.garudaindonesia.gaaccess_play.model.TempReservationHeader;
import id.co.garudaindonesia.gaaccess_play.repo.TempReservationDetailRepo;
import id.co.garudaindonesia.gaaccess_play.repo.TempReservationHeaderRepo;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@RestController
@RequestMapping(value = "/api/grab")
public class GrabController {

    private TempReservationHeaderRepo tempReservationHeaderRepo;
    private TempReservationDetailRepo tempReservationDetailRepo;

    public GrabController(TempReservationHeaderRepo tempReservationHeaderRepo, TempReservationDetailRepo tempReservationDetailRepo) {
        this.tempReservationHeaderRepo = tempReservationHeaderRepo;
        this.tempReservationDetailRepo = tempReservationDetailRepo;
    }

    @GetMapping(value = "/submit")
    private void submitGrab() throws IOException, ParseException, MessagingException {
        List<TempReservationHeader> reservations = tempReservationHeaderRepo.findAll();
        LocalDateTime localDate = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yy");
//        DateTimeFormatter filename_date = DateTimeFormatter.ofPattern("ddmmyy");
//        DateTimeFormatter filename_time = DateTimeFormatter.ofPattern("hs");

        System.out.print(localDate.format(formatter));

        //  Excel Write

        Workbook workbook = new XSSFWorkbook();
        Cell headerCell;
        Cell cell;

        Sheet sheet = workbook.createSheet("Order_18_01_19");

        Row header = sheet.createRow(0);    //  This is for Header

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        headerCell = header.createCell(0);
        headerCell.setCellValue("Date");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("service_type");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("order_id");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("origin_latitude");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(4);
        headerCell.setCellValue("origin_longitude");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(5);
        headerCell.setCellValue("origin_address");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(6);
        headerCell.setCellValue("origin_address_name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(7);
        headerCell.setCellValue("destination_latitude");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(8);
        headerCell.setCellValue("destination_longitude");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(9);
        headerCell.setCellValue("destination_address");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(10);
        headerCell.setCellValue("destination_address_name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(11);
        headerCell.setCellValue("sender_firstname");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(12);
        headerCell.setCellValue("sender_lastname"); //  sender_lastname if null set to 1
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(13);
        headerCell.setCellValue("sender_phone");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(14);
        headerCell.setCellValue("sender_email");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(15);
        headerCell.setCellValue("sender_instruction");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(16);
        headerCell.setCellValue("recipient_firstname");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(17);
        headerCell.setCellValue("recipient_lastname"); //  sender_lastname if null set to 1
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(18);
        headerCell.setCellValue("recipient_phone");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(19);
        headerCell.setCellValue("recipient_email");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(20);
        headerCell.setCellValue("order_time");
        headerCell.setCellStyle(headerStyle);

        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        int counter = 1;

        for (TempReservationHeader item : reservations) {
            //Integer pos = counter+1;
            Row row = sheet.createRow(counter);

            cell = row.createCell(0);
            cell.setCellValue(item.getCreated_at().format(formatter));
            cell.setCellStyle(style);

            cell = row.createCell(1);
            cell.setCellValue("SAME_DAY");
            cell.setCellStyle(style);

            cell = row.createCell(2);
            cell.setCellValue(item.getTempBookingNo());
            cell.setCellStyle(style);

            cell = row.createCell(3);
            cell.setCellValue(item.getOriginLat());
            cell.setCellStyle(style);

            cell = row.createCell(4);
            cell.setCellValue(item.getOriginLong());
            cell.setCellStyle(style);

            cell = row.createCell(5);
            cell.setCellValue(item.getOriginAddress());
            cell.setCellStyle(style);

            cell = row.createCell(6);
            cell.setCellValue(item.getOriginAddress());
            cell.setCellStyle(style);

            cell = row.createCell(7);
            cell.setCellValue(item.getDestinationLat());
            cell.setCellStyle(style);

            cell = row.createCell(8);
            cell.setCellValue(item.getDestinationLong());
            cell.setCellStyle(style);

            cell = row.createCell(9);
            cell.setCellValue(item.getDestinationAddress());
            cell.setCellStyle(style);

            cell = row.createCell(10);
            cell.setCellValue(item.getDestinationAddress());
            cell.setCellStyle(style);

            cell = row.createCell(11);
            cell.setCellValue("Andri Ari");
            cell.setCellStyle(style);

            cell = row.createCell(12);
            cell.setCellValue("Pratama");
            cell.setCellStyle(style);

            cell = row.createCell(13);
            cell.setCellValue("08117777804");
            cell.setCellStyle(style);

            cell = row.createCell(14);
            cell.setCellValue("andri.ari@garuda-indonesia.com");
            cell.setCellStyle(style);

            cell = row.createCell(15);
            cell.setCellValue(1);
            cell.setCellStyle(style);

            cell = row.createCell(16);
            cell.setCellValue("Grab");
            cell.setCellStyle(style);

            cell = row.createCell(17);
            cell.setCellValue("Driver");
            cell.setCellStyle(style);

            cell = row.createCell(18);
            cell.setCellValue("0812345678");
            cell.setCellStyle(style);

            cell = row.createCell(19);
            cell.setCellValue("grabdriver@grabtaxi.com");
            cell.setCellStyle(style);

            cell = row.createCell(20);
            cell.setCellValue("8:30");
            cell.setCellStyle(style);

            counter++;
        }

        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        String fileLocation = path.substring(0, path.length() - 1) + "GA_GRAB_ORDER.xlsx";

        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();

        sendEmail2Grab(fileLocation);
    }

    private void sendEmail2Grab(String filePath) throws MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("andriaripratama@gmail.com", "andri@google1");
            }
        });

        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("andriaripratama@gmail.com", true));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("ikhwanul.hakim@garuda-indonesia.com"));
        msg.setSubject("kirimpaket - GrabExpress Order");
        String messageContent = "Hi!<br> This is our order for GrabExpress! Please proceed immediately!";
        msg.setContent(messageContent, "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(messageContent, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        MimeBodyPart attachPart = new MimeBodyPart();

        attachPart.attachFile(filePath);
        multipart.addBodyPart(attachPart);
        msg.setContent(multipart);

        Transport.send(msg);
    }
}

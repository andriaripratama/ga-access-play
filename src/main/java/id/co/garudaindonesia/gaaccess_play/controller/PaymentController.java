package id.co.garudaindonesia.gaaccess_play.controller;

import com.google.gson.Gson;
import id.co.garudaindonesia.gaaccess_play.model.*;
import id.co.garudaindonesia.gaaccess_play.repo.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(value = "/api/payment")
public class PaymentController {

    private String MALLID = "6762";
    private String SHAREDKEY = "VBjCHl1xu680";

    private String CURRENCY = "360";
    private String PURCHASECURRENCY = "360";
    private String CHAINMERCHANT = "NA";

    private Gson gson;
    private HashMap<String, String> responseMessage;

    private ReservationHeaderRepo reservationHeaderRepo;
    private ReservationDetailRepo reservationDetailRepo;
    private TempReservationHeaderRepo tempReservationHeaderRepo;
    private TempReservationDetailRepo tempReservationDetailRepo;
    private ApplicationUserRepo applicationUserRepo;
    private GuestReservationRepo guestReservationRepo;
    private PaymentRepo paymentRepo;

    public PaymentController(Gson gson, HashMap<String, String> responseMessage, ReservationHeaderRepo reservationHeaderRepo, ReservationDetailRepo reservationDetailRepo, TempReservationHeaderRepo tempReservationHeaderRepo, TempReservationDetailRepo tempReservationDetailRepo, ApplicationUserRepo applicationUserRepo, GuestReservationRepo guestReservationRepo, PaymentRepo paymentRepo) {
        this.gson = gson;
        this.responseMessage = responseMessage;
        this.reservationHeaderRepo = reservationHeaderRepo;
        this.reservationDetailRepo = reservationDetailRepo;
        this.tempReservationHeaderRepo = tempReservationHeaderRepo;
        this.tempReservationDetailRepo = tempReservationDetailRepo;
        this.applicationUserRepo = applicationUserRepo;
        this.guestReservationRepo = guestReservationRepo;
        this.paymentRepo = paymentRepo;
    }

    @PostMapping
    private void doPaymentHosted(@RequestBody String request, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type","application/json");

        Payment payment = new Payment();
        GuestReservationDetail guest;
        JSONObject params = new JSONObject(request);

        String bookingNo = params.getString("bookingNo");
        String userCode = params.getString("userCode");
        ApplicationUser user = applicationUserRepo.findByCode(userCode);

        TempReservationHeader header = tempReservationHeaderRepo.findByTempBookingNo(bookingNo);
//        List<TempReservationDetail> detailList = tempReservationDetailRepo.findByTempBookingNo(bookingNo);

        double totalCost = 0.00;
        DecimalFormat df2 = new DecimalFormat(".00");

        totalCost = Double.parseDouble(header.getEstimationCost());

//        for (TempReservationDetail item : detailList) {
//
//        }

        JSONObject paymentParam = new JSONObject();

        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String REQUESTDATE = currentTime.format(formatter);

        String AMOUNT = df2.format(totalCost);
        String BASKET = "KIRIMPAKET SERVICE,"+ df2.format(totalCost) +",1,"+ df2.format(totalCost);
        String WORDS = AMOUNT+MALLID+SHAREDKEY+bookingNo;
        WORDS = DigestUtils.sha1Hex(WORDS);

        String sessionId = UUID.randomUUID().toString();

        paymentParam.put("MALLID", MALLID);
        paymentParam.put("CHAINMERCHANT", CHAINMERCHANT);
        paymentParam.put("AMOUNT", df2.format(totalCost));
        paymentParam.put("PURCHASEAMOUNT", df2.format(totalCost));
        paymentParam.put("TRANSIDMERCHANT", bookingNo);
        paymentParam.put("WORDS", WORDS);
        paymentParam.put("REQUESTDATETIME", REQUESTDATE);
        paymentParam.put("CURRENCY", CURRENCY);
        paymentParam.put("PURCHASECURRENCY", PURCHASECURRENCY);
        paymentParam.put("SESSIONID", sessionId);

        if (user != null) {
            paymentParam.put("NAME", user.getFullName());
            paymentParam.put("EMAIL", user.getEmail());
        } else {
            guest = guestReservationRepo.findByUuid(userCode);

            paymentParam.put("NAME", guest.getSenderName());
            paymentParam.put("EMAIL", guest.getSenderEmail());
        }

        paymentParam.put("BASKET", BASKET);

        //  Save to Payment Table
        payment.setTransidMerchant(bookingNo);
        payment.setTotalAmount(totalCost);
        payment.setWords(WORDS);
        payment.setSessionId(sessionId);
        long now = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(30);
        Date limit = new Date(now);
        payment.setPaymentInMilis(now);
        payment.setPaymentTimeLimit(limit);

        paymentRepo.save(payment);

        response.getWriter().write(paymentParam.toString());
    }

    @PostMapping(value = "/notify")
    private void paymentNotification(@RequestBody String request) {
        System.out.println(request);

        JSONObject payGateRespond = new JSONObject(request);

        Payment payment = new Payment();
        payment.setRawNotify(payGateRespond.toString());
        paymentRepo.save(payment);
    }
}

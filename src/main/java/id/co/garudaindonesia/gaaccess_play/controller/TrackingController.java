package id.co.garudaindonesia.gaaccess_play.controller;

import id.co.garudaindonesia.gaaccess_play.model.Pickup;
import id.co.garudaindonesia.gaaccess_play.model.TempReservationHeader;
import id.co.garudaindonesia.gaaccess_play.model.Tracking;
import id.co.garudaindonesia.gaaccess_play.repo.PickupRepo;
import id.co.garudaindonesia.gaaccess_play.repo.TempReservationHeaderRepo;
import id.co.garudaindonesia.gaaccess_play.repo.TrackingRepo;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/api/tracking")
public class TrackingController {

    private PickupRepo pickupRepo;
    private TempReservationHeaderRepo reservationHeaderRepo;
    private TrackingRepo trackingRepo;
    private HashMap<String, Object> responseMessage;

    public TrackingController(PickupRepo pickupRepo, TempReservationHeaderRepo reservationHeaderRepo, TrackingRepo trackingRepo, HashMap<String, Object> responseMessage) {
        this.pickupRepo = pickupRepo;
        this.reservationHeaderRepo = reservationHeaderRepo;
        this.trackingRepo = trackingRepo;
        this.responseMessage = responseMessage;
    }

    @GetMapping(value = "/{bookingNo}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Tracking getTracking(@PathVariable String bookingNo, @RequestHeader(value = "Access-UserCode") String userCode) throws IOException {
        TempReservationHeader reservationHeader = reservationHeaderRepo.findByTempBookingNoAndUserCode(bookingNo, userCode);

        if (reservationHeader != null) {
            Tracking tracking = trackingRepo.findTopByBookingNoOrderById(bookingNo);
            return tracking;
        } else {
            return null;
        }
    }

    @PostMapping(value = "/{bookingNo}", produces = MediaType.APPLICATION_JSON_VALUE)
    private void newTracking(@PathVariable String bookingNo, @RequestBody String body, HttpServletResponse response) throws IOException {
//        Require:
//        driverId
//        vendorCode
//        bookingNo

        JSONObject param = new JSONObject(body);

        Pickup pickup = pickupRepo.findByVendorCodeAndBookingNoAndDriverId(param.getString("vendorCode"),
                bookingNo,
                param.getString("driverId"));

        if (pickup != null) {

            Tracking tracking = new Tracking();
            tracking.setVendorCode(pickup.getVendorCode());
            tracking.setBookingNo(pickup.getBookingNo());
            tracking.setLatitude(param.getFloat("latitude"));
            tracking.setLongitude(param.getFloat("longitude"));
            trackingRepo.save(tracking);

            response.setStatus(HttpServletResponse.SC_CREATED);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}

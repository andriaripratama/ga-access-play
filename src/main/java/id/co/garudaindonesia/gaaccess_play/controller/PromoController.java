package id.co.garudaindonesia.gaaccess_play.controller;

import id.co.garudaindonesia.gaaccess_play.model.Promo;
import id.co.garudaindonesia.gaaccess_play.repo.PromoRepo;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.mail.Multipart;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "/api/promo")
public class PromoController {

    private PromoRepo promoRepo;

    public PromoController(PromoRepo promoRepo) {
        this.promoRepo = promoRepo;
    }

    @GetMapping(value = "/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    private JSONObject getPromo(@PathVariable String type, HttpServletResponse response) {
        List<Promo> promos = null;

        switch (type) {
            case "all":
                promos = promoRepo.findAll();
                break;
            case "billboard":
                promos = promoRepo.findByPromoType(type);
                break;
            case "banner":
                promos = promoRepo.findByPromoType(type);
                break;
            default:
                return null;
        }

        response.setStatus(HttpServletResponse.SC_FOUND);
        return new JSONObject(promos);
    }

    @PostMapping(value = "/new", produces = MediaType.APPLICATION_JSON_VALUE)
    private void newPromo(@RequestParam(value = "image") Multipart image, @RequestBody String body, HttpServletResponse response) {
        JSONObject param = new JSONObject(body);
        Promo promo = new Promo();

        promo.setPromoName(param.getString("name"));
        promo.setPromoType(param.getString("type"));
        promo.setPromoURL(param.getString("url"));
        promo.setPromoImage("");

        promoRepo.save(promo);

        response.setStatus(HttpServletResponse.SC_ACCEPTED);

    }
}

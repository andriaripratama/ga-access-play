package id.co.garudaindonesia.gaaccess_play.controller;

import com.google.gson.Gson;
import com.lambdaworks.crypto.SCryptUtil;
import id.co.garudaindonesia.gaaccess_play.model.*;
import id.co.garudaindonesia.gaaccess_play.repo.*;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/vendor")
public class VendorController {

    private VendorRepo vendorRepo;
    private VendorBranchRepo vendorBranchRepo;
    private VendorProductRepo vendorProductRepo;
    private VendorCoreRepo vendorCoreRepo;
    private PartnerRepo partnerRepo;
    private TempReservationHeaderRepo tempReservationHeaderRepo;
    private TempReservationDetailRepo tempReservationDetailRepo;
    private HashMap<String,String> responseMessage;
    private Gson gson;

    public VendorController(VendorRepo vendorRepo, VendorBranchRepo vendorBranchRepo, VendorProductRepo vendorProductRepo, VendorCoreRepo vendorCoreRepo, PartnerRepo partnerRepo, TempReservationHeaderRepo tempReservationHeaderRepo, TempReservationDetailRepo tempReservationDetailRepo, HashMap<String, String> responseMessage, Gson gson) {
        this.vendorRepo = vendorRepo;
        this.vendorBranchRepo = vendorBranchRepo;
        this.vendorProductRepo = vendorProductRepo;
        this.vendorCoreRepo = vendorCoreRepo;
        this.partnerRepo = partnerRepo;
        this.tempReservationHeaderRepo = tempReservationHeaderRepo;
        this.tempReservationDetailRepo = tempReservationDetailRepo;
        this.responseMessage = responseMessage;
        this.gson = gson;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> getVendor() {
        List<Vendor> vendors = vendorRepo.findAll();

        if (vendors.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            return ResponseEntity.ok(vendors);
        }
    }

    @PostMapping
    @ResponseBody
    public void newVendor(@RequestBody Vendor vendor, HttpServletResponse response) throws IOException {
        if (vendor.getName() == null || vendor.getPic() == null || vendor.getPhoneNumber() == null || vendor.getEmailAddress() == null) {
            responseMessage.put("message", "Name, PIC, Phone Number & Email Address");
            response.setHeader("Content-Type","application/json");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {
            vendor.setStatus("O");
            vendorRepo.save(vendor);

            responseMessage.put("message", "Vendor created!");
            response.setHeader("Content-Type","application/json");
            response.getWriter().write(gson.toJson(responseMessage));
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> getVendorById(@PathVariable Long id) {
        Optional<Vendor> vendor = vendorRepo.findById(id);

        if (!vendor.isPresent()) {
            responseMessage.put("message","Vendor not found");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(gson.toJson(responseMessage));
        } else {
            return ResponseEntity.ok(vendor);
        }
    }

    @PostMapping(value = "/{id}")
    public void updateVendor(@PathVariable Long id, @RequestBody Vendor request, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type","application/json");
        Optional<Vendor> vendor = vendorRepo.findById(id);

        if (!vendor.isPresent()) {
            responseMessage.put("message", "Vendor doesn't exist");

            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {
            Vendor data = vendor.get();
            request.setId(id);
            request.setCreatedAt(data.getCreatedAt());
            vendorRepo.save(request);

            responseMessage.put("message", "Vendor Updated!");
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.getWriter().write(gson.toJson(responseMessage));
        }
    }

    @PostMapping(value = "/{id}/product")
    public void newVendorProduct(@PathVariable Long id, @RequestBody VendorProduct request, HttpServletResponse response) throws IOException {
        VendorProduct product = null;
        Optional<Vendor> vendor = Optional.empty();
        response.setHeader("Content-Type", "application/json");

        if (request.getVendorCode() == null || request.getProductType() == null) {

            responseMessage.put("message", "Vendor Code and Product Type Code are required");

            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));

            return;
        } else {
            vendor = vendorRepo.findById(id);
        }

        if (!vendor.isPresent()) {

            responseMessage.put("message", "Vendor doesn't exist");

            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));

            return;
        } else {
            product = vendorProductRepo.findByVendorCodeAndProductType(request.getVendorCode(), request.getProductType());
        }

        if (product != null) {

            responseMessage.put("message", "Product already exist on this Vendor");

            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));

            return;
        } else {
            vendorProductRepo.save(request);

            responseMessage.put("message", "Product saved!");
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.getWriter().write(gson.toJson(responseMessage));
        }
    }

    @PostMapping(value = "/branch")
    public void vendorBranch(@RequestBody VendorBranch vendorBranch, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        if (vendorRepo.findByCode(vendorBranch.getVendorCode()) == null) {
            responseMessage.put("message", "Vendor doesn't exist");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {
            vendorBranchRepo.save(vendorBranch);

            responseMessage.put("message", "Vendor Branch saved!");
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.getWriter().write(gson.toJson(responseMessage));
        }
    }

    @GetMapping(value = "/nearby/{lat}/{longitude}", produces = "application/json")
    public List<NearbyVendor> nearbyVendor(@PathVariable Float lat, @PathVariable Float longitude) {
        //response.setHeader("Content-Type", "application/json");

        List<NearbyVendor> branches = vendorBranchRepo.findNearbyBranch(lat,longitude,5000);

        return branches;
        //response.setStatus(HttpServletResponse.SC_ACCEPTED);
        //response.getWriter().write(gson.toJson(branches));
    }

    @GetMapping(value = "/reservation/{id}", produces = "application/json")
    private void getDetailReservation(@PathVariable String id, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        TempReservationHeader header = tempReservationHeaderRepo.findByTempBookingNo(id);

        List<TempReservationDetail> details = tempReservationDetailRepo.findByTempBookingNo(id);

        JSONObject datas = new JSONObject();
        JSONObject head = new JSONObject(header);

        datas.put("header", head);
        datas.put("detail", details);

        response.getWriter().write(datas.toString());
    }

    @GetMapping(value = "/application/{vendorCode}", produces = "application/json")
    private Partner getVendorApplication(@PathVariable String vendorCode, HttpServletResponse response) throws IOException {
        Partner partner = partnerRepo.findByVendorCode(vendorCode);

        if (partner != null) {
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            return partner;
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return partner;
        }

    }

    @PostMapping(value = "/application", produces = "application/jsom")
    private void newVendorApplication(@RequestBody String body, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");
        JSONObject param = new JSONObject(body);
        Vendor vendor = vendorRepo.findByCode(param.getString("vendorCode"));

        if (vendor != null) {
            String vendorPassword = vendor.getCode()+System.currentTimeMillis();

            String generatedAppKey = SCryptUtil.scrypt(vendorPassword,16,16,16);

            Partner partner = new Partner();
            partner.setVendorCode(vendor.getCode());
            partner.setPartnerKey(generatedAppKey);
            partnerRepo.save(partner);

            responseMessage.put("status", "Success");
            responseMessage.put("vendor_code", vendor.getCode());
            responseMessage.put("vendor_key", generatedAppKey);

            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().write("");
        }
    }

    @PostMapping(value = "/application/endpoint", produces = MediaType.APPLICATION_JSON_VALUE)
    private void newVendorEndpoint(@RequestBody String body, HttpServletResponse response) throws IOException {
        JSONObject param = new JSONObject(body);
        Vendor vendor = vendorRepo.findByCode(param.getString("vendorCode"));

        if (vendor != null) {
            VendorCore vendorCore = new VendorCore();
            vendorCore.setBookingEndpoint(param.getString("reservationEndpoint"));
            vendorCore.setAddressEndpoint(param.getString("addressEndpoint"));
            vendorCore.setPricingEndpoint(param.getString("costEndpoint"));
            vendorCoreRepo.save(vendorCore);

            responseMessage.put("status", "Success");

            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}

package id.co.garudaindonesia.gaaccess_play.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import id.co.garudaindonesia.gaaccess_play.model.*;
import id.co.garudaindonesia.gaaccess_play.repo.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api")
public class APIController {

    private ApplicationUserRepo applicationUserRepo;
    private ProductRepo productRepo;
    private ServiceRepo serviceRepo;
    private CargoTypeRepo cargoTypeRepo;
    private VendorProductRepo vendorProductRepo;
    private BookingStatusRepo bookingStatusRepo;
    private AppVersionRepo appVersionRepo;
    private HashMap<String, String> responseMessage;
    private Gson gson;

    public APIController(ApplicationUserRepo applicationUserRepo, ProductRepo productRepo, ServiceRepo serviceRepo, CargoTypeRepo cargoTypeRepo, VendorProductRepo vendorProductRepo, BookingStatusRepo bookingStatusRepo, AppVersionRepo appVersionRepo, HashMap<String, String> responseMessage, Gson gson) {
        this.applicationUserRepo = applicationUserRepo;
        this.productRepo = productRepo;
        this.serviceRepo = serviceRepo;
        this.cargoTypeRepo = cargoTypeRepo;
        this.vendorProductRepo = vendorProductRepo;
        this.bookingStatusRepo = bookingStatusRepo;
        this.appVersionRepo = appVersionRepo;
        this.responseMessage = responseMessage;
        this.gson = gson;
    }

    @GetMapping(value = "/app_update")
    public void getAppVersion(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        String latest = appVersionRepo.getLatestVersion("mobile");

        responseMessage.put("appVersion", latest);
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        response.getWriter().write(gson.toJson(responseMessage));
    }

    @GetMapping(value = "/users")
    public ResponseEntity<?> getUsers() {
        List<ApplicationUser> users = applicationUserRepo.findAll();
        return ResponseEntity.ok(users);
    }

    @GetMapping(value = "/user/{email}")
    @ResponseBody
    public ResponseEntity<?> getUserByEmail(@PathVariable String email) {
        ApplicationUser user = applicationUserRepo.findByEmail(email);

        if (user == null) {
            responseMessage.put("message", "User doesn't exist");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(gson.toJson(responseMessage));
            //return new ResponseEntity<>(gson.toJson(responseMessage),null,HttpStatus.NO_CONTENT);
        } else {
            return ResponseEntity.ok(user);
        }

    }

    //  Service

    @GetMapping(value = "/service")
    private ResponseEntity<?> getServices() {
        List<Service> services = serviceRepo.findAll();

        if (services.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            return ResponseEntity.ok(services);
        }
    }

    @PostMapping(value = "/service")
    private void newService(@RequestBody Service service, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type","application/json");

        if (service.getCode() == null || service.getDescription() == null) {
            responseMessage.put("message", "Field Code and Description are required");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {

            if (serviceRepo.findByCode(service.getCode()) != null) {
                responseMessage.put("message", "Service already exist");
                response.getWriter().write(gson.toJson(responseMessage));
            } else {
                serviceRepo.save(service);

                responseMessage.put("message", "Service created");
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                response.getWriter().write(gson.toJson(responseMessage));
            }


        }
    }

    @RequestMapping(value = "/service/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    private ResponseEntity<?> getUpdateService(@PathVariable Long id, HttpServletRequest request) throws IOException {
        //response.setHeader("Content-Type","application/json");
        Optional<Service> service = serviceRepo.findById(id);

        if (HttpMethod.GET.matches(request.getMethod())) {

            if (!service.isPresent()) {
                responseMessage.put("message", "Service doesn't exist");
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
            } else {
                Service servData = service.get();
                return ResponseEntity.ok(servData);
            }

        } else if (HttpMethod.POST.matches(request.getMethod())) {

            ObjectMapper mapper = new ObjectMapper();
            Service reqParam = mapper.readValue(request.getInputStream(), Service.class);

            if (!service.isPresent()) {
                responseMessage.put("message", "Service doesn't exist");
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
            } else {
                Service servData = service.get();

                reqParam.setId(id);
                reqParam.setCreatedAt(servData.getCreatedAt());

                serviceRepo.save(reqParam);

                responseMessage.put("message", "Service updated!");
                return ResponseEntity.ok(responseMessage);
            }

        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
    }

    //  Product

    @GetMapping(value = "/product")
    private ResponseEntity<?> getProducts() {
        List<Product> products = productRepo.findAll();

        if (products.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            return ResponseEntity.ok(products);
        }
    }

    @PostMapping(value = "/product")
    private void newProduct(@RequestBody Product product, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type","application/json");

        if (product.getServiceCode() == null || product.getDescription() == null) {
            responseMessage.put("message", "Field Service Code and Description are required");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {

            if (productRepo.findByCode(product.getCode()) != null) {
                responseMessage.put("message", "Product already exist");
                response.getWriter().write(gson.toJson(responseMessage));
            } else {
                productRepo.save(product);

                responseMessage.put("message", "Product created");
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                response.getWriter().write(gson.toJson(responseMessage));
            }


        }
    }

    @PostMapping(value = "/product/vendor", produces = "application/json")
    private ResponseEntity<?> getProductByVendor(@RequestBody Map<String, Object> request, HttpServletResponse response) throws IOException {
        List<ProductByVendor> vendors = vendorProductRepo.findByProductType(request.get("productCode").toString());

        if (vendors == null || vendors.isEmpty()) {
            responseMessage.put("message", "There's no vendor for this product");
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(responseMessage);
        } else {
//            JSONArray items = new JSONArray();
//
//            for (ProductByVendor vendor : vendors) {
//                JSONObject item = new JSONObject();
//                //System.out.println(vendor.getVendorCode());
//                item.put("id", vendor.getId());
//                item.put("code", vendor.getCode());
//                item.put("vendor_code", vendor.getVendorCode());
//                item.put("vendor_cost", "149000");
//                items.put(item);
//            }

            return ResponseEntity.ok(vendors);
        }
    }

    //  Cargo Type

    @GetMapping(value = "/cargo_type")
    private ResponseEntity<?> getCargoTypes() {
        List<CargoType> cargoTypes = cargoTypeRepo.findAll();

        if (cargoTypes.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            return ResponseEntity.ok(cargoTypes);
        }
    }

    @PostMapping(value = "/cargo_type")
    private void newCargoType(@RequestBody CargoType cargoType, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        if (cargoType.getCode() == null || cargoType.getDescription() == "") {
            responseMessage.put("message", "Field Cargo Type Code and Description are required");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {

            if (cargoTypeRepo.findByCode(cargoType.getCode()) != null) {
                responseMessage.put("message", "Cargo Type already exist");
                response.getWriter().write(gson.toJson(responseMessage));
            } else {
                cargoTypeRepo.save(cargoType);

                responseMessage.put("message", "Cargo Type created");
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
                response.getWriter().write(gson.toJson(responseMessage));
            }

        }
    }

    @GetMapping(value = "/booking_status", produces = "application/json")
    private List<BookingStatus> getBookingStatus() {

        List<BookingStatus> statusList = bookingStatusRepo.findAll();

        return statusList;
    }

    @PostMapping(value = "/booking_status")
    private void newBookingStatus(@RequestBody String request, HttpServletResponse response) throws IOException {

        JSONObject params = new JSONObject(request);
        response.setHeader("Content-Type", "application/json");
        BookingStatus isExist = bookingStatusRepo.findByCode(params.getString("code"));

        if (isExist != null) {
            responseMessage.put("status", "Booking Status Code already exist");
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {
            BookingStatus bookingStatus = new BookingStatus();
            bookingStatus.setCode(params.getString("code"));
            bookingStatus.setDescription(params.getString("desc"));
            bookingStatus.setIconUrl(params.getString("iconUrl"));

            bookingStatusRepo.save(bookingStatus);

            responseMessage.put("status", "Booking Status created!");
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.getWriter().write(gson.toJson(responseMessage));
        }
    }

}

package id.co.garudaindonesia.gaaccess_play.controller;

import com.google.gson.Gson;
import id.co.garudaindonesia.gaaccess_play.model.*;
import id.co.garudaindonesia.gaaccess_play.repo.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/api/reservation")
public class ReservationController {

    private Gson gson;
    private TempReservationHeaderRepo tempReservationHeaderRepo;
    private TempReservationDetailRepo tempReservationDetailRepo;
    private ReservationStatusRepo reservationStatusRepo;
    private ReservationHeaderRepo reservationHeaderRepo;
    private ReservationDetailRepo reservationDetailRepo;
    private GuestReservationRepo guestReservationRepo;
    private VendorProductRepo vendorProductRepo;
    private HashMap<String, String> responseMessage;

    public ReservationController(Gson gson, TempReservationHeaderRepo tempReservationHeaderRepo, TempReservationDetailRepo tempReservationDetailRepo, ReservationStatusRepo reservationStatusRepo, ReservationHeaderRepo reservationHeaderRepo, ReservationDetailRepo reservationDetailRepo, GuestReservationRepo guestReservationRepo, VendorProductRepo vendorProductRepo, HashMap<String, String> responseMessage) {
        this.gson = gson;
        this.tempReservationHeaderRepo = tempReservationHeaderRepo;
        this.tempReservationDetailRepo = tempReservationDetailRepo;
        this.reservationStatusRepo = reservationStatusRepo;
        this.reservationHeaderRepo = reservationHeaderRepo;
        this.reservationDetailRepo = reservationDetailRepo;
        this.guestReservationRepo = guestReservationRepo;
        this.vendorProductRepo = vendorProductRepo;
        this.responseMessage = responseMessage;
    }

    @GetMapping(value = "/all", produces = "application/json")
    private void getAllReservation(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        List<TempReservationHeader> allReservation = tempReservationHeaderRepo.getAllReservation();

        String reservations = gson.toJson(allReservation);

        response.getWriter().write(reservations);
    }

    @GetMapping(value = "/user_reservation", produces = "application/json")
    private ResponseEntity<?> getUserReservation(@RequestHeader(value = "Access-UserCode") String userCode) {
        List<TempReserveHeaderTrack> userReservation = null;
        List<TempReserveHeaderGuest> guestReservation = null;

        if (userCode.length() > 8) {
            guestReservation = tempReservationHeaderRepo.getGuestReservation(userCode);
            return ResponseEntity.ok(guestReservation);
        } else {
            userReservation = tempReservationHeaderRepo.getUserReservation(userCode);
            return ResponseEntity.ok(userReservation);
        }

    }

    @GetMapping(value = "/{id}", produces = "application/json")
    private void getDetailReservation(@PathVariable String id, @RequestHeader(value = "Access-UserCode") String code, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        TempReservationHeader header = tempReservationHeaderRepo.findByTempBookingNoAndUserCode(id, code);
        List<TempReservationDetail> details = tempReservationDetailRepo.findByTempBookingNo(id);
        ReservationStatusDetail statuses = reservationStatusRepo.getLatestStatus(id);

        JSONObject datas = new JSONObject();
        JSONObject head = new JSONObject(header);
        JSONObject status = new JSONObject(statuses);

        datas.put("header", head);
        datas.put("detail", details);
        datas.put("status", status);

        response.getWriter().write(datas.toString());
    }

    @GetMapping(value = "/{id}/status", produces = "application/json")
    private void getDetailReservationStatus(@PathVariable String id, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        List<ReservationStatusDetail> statuses = reservationStatusRepo.getDetailStatus(id);

        JSONObject datas = new JSONObject();

        datas.put("status", statuses);

        response.getWriter().write(datas.toString());
    }

    @PostMapping(value = "/update/{bookingId}")
    private void updateByVendor(@PathVariable String bookingId, @RequestBody String request, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        JSONObject params = new JSONObject(request);

        ReservationHeader isConfirmed = reservationHeaderRepo.findByBookingNo(bookingId);

        if (isConfirmed != null) {
            responseMessage.put("status", "Booking already updated!");
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {

            Integer isBookingExist = tempReservationHeaderRepo.checkBookingExist(bookingId, params.getString("userCode"));

            if (isBookingExist == 1) {
                tempReservationHeaderRepo.setBookingStatus(params.getString("status"), params.getString("userCode"), bookingId);
                Integer confirmBooking = tempReservationHeaderRepo.confirmBooking(bookingId, params.getString("userCode"));

                switch (confirmBooking) {
                    case 0:
                        responseMessage.put("status", "Failed to confirm booking.");
                        response.setStatus(HttpServletResponse.SC_CONFLICT);
                        break;
                    case 1:

                        //  Saving to Status History
                        ReservationStatus status = new ReservationStatus();
                        status.setBookingNo(bookingId);
                        status.setBookingStatus(params.getString("status"));
                        reservationStatusRepo.save(status);

                        responseMessage.put("status", "Success!");
                        response.setStatus(HttpServletResponse.SC_ACCEPTED);
                        break;
                }

                response.getWriter().write(gson.toJson(responseMessage));
            } else {
                responseMessage.put("status", "Booking No. not found");
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                response.getWriter().write(gson.toJson(responseMessage));
            }

        }

    }

    @PostMapping(value = "/update/item/{bookingId}")
    private void updateItemByVendor(@PathVariable String bookingId, @RequestBody String request, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        JSONObject data = new JSONObject(request);
        ReservationHeader booking = reservationHeaderRepo.findByBookingNoAndUserCode(bookingId, data.getString("userCode"));

        if (booking == null) {
            responseMessage.put("status", "Booking No. not found");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().write(gson.toJson(responseMessage));
        } else {

            JSONArray items = data.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {

                String reserveCode = items.getJSONObject(i).getString("reserveCode");
                Integer width = items.getJSONObject(i).getInt("width");
                Integer length = items.getJSONObject(i).getInt("length");
                Integer height = items.getJSONObject(i).getInt("height");

                reservationDetailRepo.updateItemDetail(reserveCode, bookingId, length, width, height);
            }

            responseMessage.put("status", "Update success!");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));

        }
    }

    @PostMapping(value = "/book", produces = "application/json")
    private void newReservation(@RequestBody String request, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");

        JSONObject params = new JSONObject(request);
        JSONObject recipientInfo = params.getJSONObject("destinationInfo");
        JSONArray shipmentItems = params.getJSONArray("items");

        if (params.getString("serviceCode").equals("DTD")) {
            //  Door to Door

            TempReservationHeader header = new TempReservationHeader();
            header.setUserId(params.getInt("userId"));
            header.setUserCode(params.getString("userCode"));
            header.setProductType(params.getString("productType"));
            header.setServiceCode(params.getString("serviceCode"));
            header.setVendorCode(params.getString("vendorCode"));
            header.setStatus("BKD");
            header.setOriginLat(params.getString("originLat"));
            header.setOriginLong(params.getString("originLong"));
            header.setOriginAddress(params.getString("originAddress"));
            header.setDestinationLat(params.getString("destinationLat"));
            header.setDestinationLong(params.getString("destinationLong"));
            header.setDestinationAddress(params.getString("destinationAddress"));

            header.setRecipientName(recipientInfo.getString("name"));
            header.setRecipientPhone(recipientInfo.getString("phone"));

            header.setEstimationCost(params.getString("shipmentCost"));

            tempReservationHeaderRepo.save(header);

            String tempCode = tempReservationHeaderRepo.findTempBookingNo(header.getId());

            for (int i = 0; i < shipmentItems.length(); i++) {
                TempReservationDetail detail = new TempReservationDetail();
                JSONObject item = shipmentItems.getJSONObject(i);

                detail.setTempBookingNo(tempCode);
                detail.setItemDescription(item.getString("cargoName"));
                detail.setItemRemark(item.getString("cargoRemark"));
                //detail.setItemPreview(item.getString("cargoPreview"));
                detail.setCargoType(item.getString("cargoType"));
                detail.setWeight(item.getInt("weight"));

                tempReservationDetailRepo.save(detail);
            }

            //  Saving to Status History
            ReservationStatus status = new ReservationStatus();
            status.setBookingNo(tempCode);
            status.setBookingStatus("BKD");
            reservationStatusRepo.save(status);

            if (params.has("isGuest") && params.getBoolean("isGuest")) {
                GuestReservationDetail guest = new GuestReservationDetail();
                guest.setUUID(params.getString("userCode"));
                guest.setSenderName(params.getString("senderName"));
                guest.setSenderPhone(params.getString("senderPhone"));
                guest.setSenderEmail(params.getString("senderEmail"));
                guest.setSenderAddress(params.getString("senderAddress"));
                guest.setBookingNo(tempCode);

                guestReservationRepo.save(guest);
            }

            responseMessage.put("message", "Booking Created!");
            responseMessage.put("bookingNo", tempCode);

            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.getWriter().write(gson.toJson(responseMessage));

        } else if (params.getString("serviceCode").equals("PTP")) {
            //  Port to Port

            TempReservationHeader header = new TempReservationHeader();
            header.setUserId(params.getInt("userId"));
            header.setUserCode(params.getString("userCode"));
            header.setServiceCode(params.getString("serviceCode"));
            header.setVendorCode(params.getString("vendorCode"));
            header.setFlightNumber(params.getString("flightNo"));
            header.setOriginStation(params.getString("originStation"));
            header.setDestinationStation(params.getString("destinationStation"));
            header.setStatus("BKD");
            header.setRecipientName(recipientInfo.getString("name"));
            header.setRecipientPhone(recipientInfo.getString("phone"));

            header.setEstimationCost(params.getString("shipmentCost"));

            tempReservationHeaderRepo.save(header);

            String tempCode = tempReservationHeaderRepo.findTempBookingNo(header.getId());

            for (int i = 0; i < shipmentItems.length(); i++) {
                TempReservationDetail detail = new TempReservationDetail();
                JSONObject item = shipmentItems.getJSONObject(i);

                detail.setTempBookingNo(tempCode);
                detail.setItemDescription(item.getString("cargoName"));
                detail.setItemRemark(item.getString("cargoRemark"));
//                detail.setItemPreview(item.getString("cargoPreview"));
                detail.setCargoType(item.getString("cargoType"));
                detail.setWeight(item.getInt("weight"));

                tempReservationDetailRepo.save(detail);
            }

            //  Saving to Status History
            ReservationStatus status = new ReservationStatus();
            status.setBookingNo(tempCode);
            status.setBookingStatus("BKD");
            reservationStatusRepo.save(status);

            if (params.has("isGuest") && params.getBoolean("isGuest")) {
                GuestReservationDetail guest = new GuestReservationDetail();
                guest.setUUID(params.getString("userCode"));
                guest.setSenderName(params.getString("senderName"));
                guest.setSenderPhone(params.getString("senderPhone"));
                guest.setSenderEmail(params.getString("senderEmail"));
                guest.setSenderAddress(params.getString("senderAddress"));
                guest.setBookingNo(tempCode);

                guestReservationRepo.save(guest);
            }

            responseMessage.put("message", "Booking Created!");
            responseMessage.put("bookingNo", tempCode);

            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.getWriter().write(gson.toJson(responseMessage));

        } else {

            responseMessage.put("message", "Unidentified request");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            response.getWriter().write(gson.toJson(responseMessage));

        }

    }

    @PostMapping(value = "/pricing", produces = "application/json")
    private void getPricing(@RequestBody String body, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type", "application/json");
        RestTemplate restTemplate = new RestTemplate();
        JSONObject param = new JSONObject(body);

        String productCode = param.getString("productCode");
        String originLat = param.getString("originLat");
        String originLong = param.getString("originLong");
        String destinationLat = param.getString("destinationLat");
        String destinationLong = param.getString("destinationLong");
        int packageWeight = param.getInt("weight");

        String province = null;
        String city = null;
        String district = null;

        // Get the address component first
        ResponseEntity<String> geo = restTemplate.getForEntity("https://maps.googleapis.com/maps/api/geocode/json?latlng="+ originLat +","+ originLong +"&key=AIzaSyAmaKpvnAEumyDzduQI7L5AWqqyR-0LFzA&language=id", String.class);
        JSONObject reverseGeocoding = new JSONObject(geo.getBody());
        JSONArray addressComponent = reverseGeocoding.getJSONArray("results");

        for (int i = 0; i < addressComponent.length(); i++) {
            String types = addressComponent.getJSONObject(i).getJSONArray("types").getString(0);

            switch (types) {
                case "administrative_area_level_1":
                    province = addressComponent.getJSONObject(i).getJSONArray("address_components").getJSONObject(0).getString("long_name");
                    break;
                case "locality":
                    city = addressComponent.getJSONObject(i).getJSONArray("address_components").getJSONObject(0).getString("long_name");
                    break;
                case "administrative_area_level_3":
                    district = addressComponent.getJSONObject(i).getJSONArray("address_components").getJSONObject(0).getString("long_name");
                    break;
                default:
                    break;
            }
        }

        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        responseMessage.put("province", province);
        responseMessage.put("city", city);
        responseMessage.put("district", district);

        response.getWriter().write(gson.toJson(responseMessage));

//        List<ProductByVendor> vendorProducts = vendorProductRepo.findByProductType(productCode);
//
//        if (!vendorProducts.isEmpty()) {
//            response.setStatus(HttpServletResponse.SC_ACCEPTED);
//            response.getWriter().write(gson.toJson(vendorProducts));
//
//            for (ProductByVendor vendor : vendorProducts) {
//
//            }
//
//        } else {
//
//        }


    }

    @PostMapping(value = "/status/{bookingId}")
    private void updateStatus(@PathVariable String bookingId, @RequestBody String request, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Type","application/json");

        JSONObject params = new JSONObject(request);

        reservationStatusRepo.updateReservationStatus(params.getString("status"), bookingId, params.getString("userCode"));

        //  Saving to Status History
        ReservationStatus status = new ReservationStatus();
        status.setBookingNo(bookingId);
        status.setBookingStatus(params.getString("status"));
        reservationStatusRepo.save(status);

        responseMessage.put("status", "Success!");
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        response.getWriter().write(gson.toJson(responseMessage));
    }
}

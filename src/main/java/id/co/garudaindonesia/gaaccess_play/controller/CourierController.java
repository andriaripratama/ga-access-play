package id.co.garudaindonesia.gaaccess_play.controller;

import id.co.garudaindonesia.gaaccess_play.model.Courier;
import id.co.garudaindonesia.gaaccess_play.repo.CourierRepo;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/master/courier")
public class CourierController {

    private CourierRepo courierRepo;

    public CourierController(CourierRepo courierRepo) {
        this.courierRepo = courierRepo;
    }

    @GetMapping
    public ResponseEntity<?> getCourier() {
        List<Courier> couriers = courierRepo.findAll();

        if (couriers.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(couriers);
        }
    }

    @PostMapping(value = "/new")
    public ResponseEntity<?> addCourier(@RequestBody Courier courier) {
        HashMap<String,String> messages = new HashMap<>();

        try {
            courierRepo.save(courier);

            messages.put("message", "success");
            return ResponseEntity.ok(messages);

        } catch (DataAccessException e) {
            messages.put("message", "All field are required!");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(messages);
        }

    }
}

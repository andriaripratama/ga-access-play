package id.co.garudaindonesia.gaaccess_play.model;

import java.io.Serializable;

public class NearbyVendor implements Serializable {

    private Long id;
    private String branchAddress, branchName, branchOperation, latitude, longitude, vendorCode, vendorLogo;

    public NearbyVendor(Long id, String branchAddress, String branchName, String branchOperation, String latitude, String longitude, String vendorCode, String vendorLogo) {
        this.id = id;
        this.branchAddress = branchAddress;
        this.branchName = branchName;
        this.branchOperation = branchOperation;
        this.latitude = latitude;
        this.longitude = longitude;
        this.vendorCode = vendorCode;
        this.vendorLogo = vendorLogo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchOperation() {
        return branchOperation;
    }

    public void setBranchOperation(String branchOperation) {
        this.branchOperation = branchOperation;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorLogo() {
        return vendorLogo;
    }

    public void setVendorLogo(String vendorLogo) {
        this.vendorLogo = vendorLogo;
    }
}

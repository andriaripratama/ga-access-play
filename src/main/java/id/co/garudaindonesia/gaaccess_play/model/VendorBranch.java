package id.co.garudaindonesia.gaaccess_play.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mst_vendor_branch")
@SqlResultSetMapping(
        name ="getNearbyVendor",
        classes = {
                @ConstructorResult(
                        targetClass = NearbyVendor.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "branch_address", type = String.class),
                                @ColumnResult(name = "branch_name", type = String.class),
                                @ColumnResult(name = "branch_operational", type = String.class),
                                @ColumnResult(name = "latitude", type = String.class),
                                @ColumnResult(name = "longitude", type = String.class),
                                @ColumnResult(name = "vendor_code", type = String.class),
                                @ColumnResult(name = "vendor_logo", type = String.class)
                        }
                )
        }
)
@NamedNativeQuery(name = "VendorBranch.getNearbyVendor", query = "SELECT vb.*, vn.logo AS vendor_logo " +
        "FROM mst_vendor_branch vb " +
        "LEFT JOIN (SELECT code, logo FROM mst_vendor) vn ON vb.vendor_code = vn.code " +
        "WHERE earth_distance(ll_to_earth(vb.latitude, vb.longitude), ll_to_earth(?1, ?2)) < ?3", resultSetMapping = "getNearbyVendor")
public class VendorBranch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String vendorCode;

    @Column(nullable = false)
    private String branchName;

    @Column(nullable = false)
    private String branchAddress;

    private String branchOperational;

    private Float latitude;

    private Float longitude;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @CreationTimestamp
    private LocalDateTime createdAt;

    public VendorBranch() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getBranchOperational() {
        return branchOperational;
    }

    public void setBranchOperational(String branchOperational) {
        this.branchOperational = branchOperational;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}

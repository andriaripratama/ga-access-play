package id.co.garudaindonesia.gaaccess_play.model;

import java.io.Serializable;

public class ProductByVendor implements Serializable {

    private String code, productType, vendorCode, name, logo;

    public ProductByVendor(String code, String productType, String vendorCode, String name, String logo) {
        this.code = code;
        this.productType = productType;
        this.vendorCode = vendorCode;
        this.name = name;
        this.logo = logo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}

package id.co.garudaindonesia.gaaccess_play.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ReservationStatusDetail implements Serializable {

    private String bookingNo, bookingStatus, description, iconUrl;
    private LocalDateTime updated_at, created_at;

    public ReservationStatusDetail(String bookingNo, String bookingStatus, String description, String iconUrl, LocalDateTime updated_at, LocalDateTime created_at) {
        this.bookingNo = bookingNo;
        this.bookingStatus = bookingStatus;
        this.description = description;
        this.iconUrl = iconUrl;
        this.updated_at = updated_at;
        this.created_at = created_at;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }
}

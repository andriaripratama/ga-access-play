package id.co.garudaindonesia.gaaccess_play.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Blob;
import java.time.LocalDateTime;

@Entity
@Table(name = "trx_temp_reservation_detail")
public class TempReservationDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(insertable = false)
    private String tempReserveCode;

    @Column(nullable = false)
    private String tempBookingNo;

    @Column(nullable = false)
    private String itemDescription;

    private String itemRemark;

    private String itemPreview;

    @Column(nullable = false)
    private String cargoType;

    @Column(nullable = false)
    private Integer weight;

    private Integer length;

    private Integer width;

    private Integer height;

    @CreationTimestamp
    private LocalDateTime created_at;

    @UpdateTimestamp
    private LocalDateTime updated_at;

    public TempReservationDetail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTempReserveCode() {
        return tempReserveCode;
    }

    public void setTempReserveCode(String tempReserveCode) {
        this.tempReserveCode = tempReserveCode;
    }

    public String getTempBookingNo() {
        return tempBookingNo;
    }

    public void setTempBookingNo(String tempBookingNo) {
        this.tempBookingNo = tempBookingNo;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemRemark() {
        return itemRemark;
    }

    public void setItemRemark(String itemRemark) {
        this.itemRemark = itemRemark;
    }

    public String getItemPreview() {
        return itemPreview;
    }

    public void setItemPreview(String itemPreview) {
        this.itemPreview = itemPreview;
    }

    public String getCargoType() {
        return cargoType;
    }

    public void setCargoType(String cargoType) {
        this.cargoType = cargoType;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }
}

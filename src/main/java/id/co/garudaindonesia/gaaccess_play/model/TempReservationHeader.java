package id.co.garudaindonesia.gaaccess_play.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;

@Entity
@Table(name = "trx_temp_reservation_header")
@DynamicUpdate

@SqlResultSetMapping(
        name = "getReservationWithStatus",
        classes = {
                @ConstructorResult(
                        targetClass = TempReserveHeaderTrack.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "temp_booking_no", type = String.class),
                                @ColumnResult(name = "user_id", type = Integer.class),
                                @ColumnResult(name = "user_code", type = String.class),
                                @ColumnResult(name = "product_type", type = String.class),
                                @ColumnResult(name = "service_code", type = String.class),
                                @ColumnResult(name = "vendor_code", type = String.class),
                                @ColumnResult(name = "status", type = String.class),
                                @ColumnResult(name = "confirm_date_time", type = Integer.class),
                                @ColumnResult(name = "confirm_pic", type = Integer.class),
                                @ColumnResult(name = "origin_lat", type = String.class),
                                @ColumnResult(name = "origin_long", type = String.class),
                                @ColumnResult(name = "origin_address", type = String.class),
                                @ColumnResult(name = "destination_lat", type = String.class),
                                @ColumnResult(name = "destination_long", type = String.class),
                                @ColumnResult(name = "destination_address", type = String.class),
                                @ColumnResult(name = "origin_station", type = String.class),
                                @ColumnResult(name = "destination_station", type = String.class),
                                @ColumnResult(name = "flight_number", type = String.class),
                                @ColumnResult(name = "recipient_name", type = String.class),
                                @ColumnResult(name = "recipient_phone", type = String.class),
                                @ColumnResult(name = "created_at", type = LocalDateTime.class),
                                @ColumnResult(name = "updated_at", type = LocalDateTime.class),
                                @ColumnResult(name = "desc_product", type = String.class),
                                @ColumnResult(name = "desc_service", type = String.class),
                                @ColumnResult(name = "vendor_name", type = String.class),
                                @ColumnResult(name = "vendor_logo", type = String.class),
                                @ColumnResult(name = "last_status", type = String.class),
                        }
                )
        }
)
@SqlResultSetMapping(
        name = "getGuestReservationWithStatus",
        classes = {
                @ConstructorResult(
                        targetClass = TempReserveHeaderGuest.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "temp_booking_no", type = String.class),
                                @ColumnResult(name = "user_id", type = Integer.class),
                                @ColumnResult(name = "user_code", type = String.class),
                                @ColumnResult(name = "product_type", type = String.class),
                                @ColumnResult(name = "service_code", type = String.class),
                                @ColumnResult(name = "vendor_code", type = String.class),
                                @ColumnResult(name = "status", type = String.class),
                                @ColumnResult(name = "confirm_date_time", type = Integer.class),
                                @ColumnResult(name = "confirm_pic", type = Integer.class),
                                @ColumnResult(name = "origin_lat", type = String.class),
                                @ColumnResult(name = "origin_long", type = String.class),
                                @ColumnResult(name = "origin_address", type = String.class),
                                @ColumnResult(name = "destination_lat", type = String.class),
                                @ColumnResult(name = "destination_long", type = String.class),
                                @ColumnResult(name = "destination_address", type = String.class),
                                @ColumnResult(name = "origin_station", type = String.class),
                                @ColumnResult(name = "destination_station", type = String.class),
                                @ColumnResult(name = "flight_number", type = String.class),
                                @ColumnResult(name = "recipient_name", type = String.class),
                                @ColumnResult(name = "recipient_phone", type = String.class),
                                @ColumnResult(name = "created_at", type = LocalDateTime.class),
                                @ColumnResult(name = "updated_at", type = LocalDateTime.class),
                                @ColumnResult(name = "desc_product", type = String.class),
                                @ColumnResult(name = "desc_service", type = String.class),
                                @ColumnResult(name = "vendor_name", type = String.class),
                                @ColumnResult(name = "vendor_logo", type = String.class),
                                @ColumnResult(name = "last_status", type = String.class),
                                @ColumnResult(name = "sender_name", type = String.class),
                                @ColumnResult(name = "sender_phone", type = String.class),
                                @ColumnResult(name = "sender_email", type = String.class),
                                @ColumnResult(name = "sender_address", type = String.class),
                        }
                )
        }
)
@NamedNativeQuery(name = "TempReservationHeader.getBookingDetail", query = "SELECT tmpbooking.*, prd.description AS desc_product, svc.description AS desc_service, vnd.logo AS vendor_logo, vnd.name AS vendor_name, stab.description AS last_status " +
        "FROM trx_temp_reservation_header tmpbooking " +
        "LEFT JOIN (SELECT code, description FROM mst_product) prd ON tmpbooking.product_type = prd.code " +
        "LEFT JOIN (SELECT code, description FROM mst_service) svc ON tmpbooking.service_code = svc.code " +
        "LEFT JOIN (SELECT code, name, logo FROM mst_vendor) vnd ON tmpbooking.vendor_code = vnd.code " +
        "LEFT JOIN (SELECT booking_no, booking_status FROM trx_reservation_status ORDER BY id DESC LIMIT 1) sta ON tmpbooking.temp_booking_no = sta.booking_no " +
        "LEFT JOIN (SELECT code, description FROM mst_booking_status) stab ON sta.booking_status = stab.code " +
        "WHERE tmpbooking.user_code = ?1 ORDER BY tmpbooking.id DESC", resultSetMapping = "getReservationWithStatus")

@NamedNativeQuery(name = "TempReservationHeader.getGuestBookingDetail", query = "SELECT DISTINCT ON (tmpbooking.id) *, grv.sender_name, grv.sender_phone, grv.sender_email, grv.sender_address, prd.description AS desc_product, svc.description AS desc_service, vnd.logo AS vendor_logo, vnd.name AS vendor_name, stab.description AS last_status " +
        "FROM trx_temp_reservation_header tmpbooking " +
        "LEFT JOIN (SELECT uuid, sender_name, sender_phone, sender_email, sender_address FROM trx_guest_reservation ORDER BY id DESC LIMIT 1) grv ON tmpbooking.user_code = grv.uuid " +
        "LEFT JOIN (SELECT code, description FROM mst_product) prd ON tmpbooking.product_type = prd.code " +
        "LEFT JOIN (SELECT code, description FROM mst_service) svc ON tmpbooking.service_code = svc.code " +
        "LEFT JOIN (SELECT code, name, logo FROM mst_vendor) vnd ON tmpbooking.vendor_code = vnd.code " +
        "LEFT JOIN (SELECT booking_no, booking_status FROM trx_reservation_status) sta ON tmpbooking.temp_booking_no = sta.booking_no " +
        "LEFT JOIN (SELECT code, description FROM mst_booking_status) stab ON sta.booking_status = stab.code " +
        "WHERE tmpbooking.user_code = ?1 ORDER BY tmpbooking.id DESC", resultSetMapping = "getGuestReservationWithStatus")
public class TempReservationHeader {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(insertable = false)
    private String tempBookingNo;

    @Column(nullable = false)
    private Integer userId;

    @Column(nullable = false)
    private String userCode;

    //  Product Type allowed to be Null cause Port to Port doesn't have Product
    private String productType;

    @Column(nullable = false)
    private String serviceCode;

    @Column(nullable = false)
    private String vendorCode;

    @Column(nullable = false)
    private String status;

    private LocalDateTime confirmDateTime;

    private Integer confirmPic;

    //  This for Door to Door
    private String originLat;

    private String originLong;

    private String originAddress;

    private String destinationLat;

    private String destinationLong;

    private String destinationAddress;

    //  This for Port to Port
    private String originStation;

    private String destinationStation;

    private String flightNumber;

    @Column(nullable = false)
    private String recipientName;

    @Column(nullable = false)
    private String recipientPhone;

    @Column(nullable = false)
    private String estimationCost;

    private String finalCost;

    @CreationTimestamp
    private LocalDateTime created_at;

    @UpdateTimestamp
    private LocalDateTime updated_at;

    public TempReservationHeader() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTempBookingNo() {
        return tempBookingNo;
    }

    public void setTempBookingNo(String tempBookingNo) {
        this.tempBookingNo = tempBookingNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getConfirmDateTime() {
        return confirmDateTime;
    }

    public void setConfirmDateTime(LocalDateTime confirmDateTime) {
        this.confirmDateTime = confirmDateTime;
    }

    public Integer getConfirmPic() {
        return confirmPic;
    }

    public void setConfirmPic(Integer confirmPic) {
        this.confirmPic = confirmPic;
    }

    public String getOriginLat() {
        return originLat;
    }

    public void setOriginLat(String originLat) {
        this.originLat = originLat;
    }

    public String getOriginLong() {
        return originLong;
    }

    public void setOriginLong(String originLong) {
        this.originLong = originLong;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(String destinationLat) {
        this.destinationLat = destinationLat;
    }

    public String getDestinationLong() {
        return destinationLong;
    }

    public void setDestinationLong(String destinationLong) {
        this.destinationLong = destinationLong;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    public String getEstimationCost() {
        return estimationCost;
    }

    public void setEstimationCost(String estimationCost) {
        this.estimationCost = estimationCost;
    }

    public String getFinalCost() {
        return finalCost;
    }

    public void setFinalCost(String finalCost) {
        this.finalCost = finalCost;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    public String getOriginStation() {
        return originStation;
    }

    public void setOriginStation(String originStation) {
        this.originStation = originStation;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(String destinationStation) {
        this.destinationStation = destinationStation;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }


}


package id.co.garudaindonesia.gaaccess_play.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mst_vendor_core")
public class VendorCore {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String vendorCode;

    private String bookingEndpoint;

    private String pricingEndpoint;

    private String addressEndpoint;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public VendorCore() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getBookingEndpoint() {
        return bookingEndpoint;
    }

    public void setBookingEndpoint(String bookingEndpoint) {
        this.bookingEndpoint = bookingEndpoint;
    }

    public String getPricingEndpoint() {
        return pricingEndpoint;
    }

    public void setPricingEndpoint(String pricingEndpoint) {
        this.pricingEndpoint = pricingEndpoint;
    }

    public String getAddressEndpoint() {
        return addressEndpoint;
    }

    public void setAddressEndpoint(String addressEndpoint) {
        this.addressEndpoint = addressEndpoint;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

package id.co.garudaindonesia.gaaccess_play.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.parameters.P;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mst_vendor_product")
@SqlResultSetMapping(
        name = "getProductListByVendor",
        classes = {
                @ConstructorResult(
                        targetClass = ProductByVendor.class,
                        columns = {
                                @ColumnResult(name = "code", type = String.class),
                                @ColumnResult(name = "product_type", type = String.class),
                                @ColumnResult(name = "vendor_code", type = String.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "logo", type = String.class)
                        }
                )
        }
)
@NamedNativeQuery(name = "VendorProduct.getAvailableVendorByProduct", query = "SELECT product.code, product.product_type, product.vendor_code, vendor.name, vendor.logo " +
        "FROM mst_vendor_product product, mst_vendor vendor WHERE product.product_type = ?1 " +
        "AND vendor.status = 'A' AND product.vendor_code = vendor.code", resultSetMapping = "getProductListByVendor")
public class VendorProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private String code;

    @Column(nullable = false)
    private String vendorCode;

    @Column(nullable = false)
    private String productType;

    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @JsonIgnore
    private LocalDateTime updatedAt;

    public VendorProduct() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

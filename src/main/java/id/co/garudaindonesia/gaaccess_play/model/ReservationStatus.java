package id.co.garudaindonesia.gaaccess_play.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "trx_reservation_status")
@SqlResultSetMapping(
        name = "getReservationStatusDetail",
        classes = {
                @ConstructorResult(
                        targetClass = ReservationStatusDetail.class,
                        columns = {
                                @ColumnResult(name = "booking_no", type = String.class),
                                @ColumnResult(name = "booking_status", type = String.class),
                                @ColumnResult(name = "description", type = String.class),
                                @ColumnResult(name = "icon_url", type = String.class),
                                @ColumnResult(name = "updated_at", type = LocalDateTime.class),
                                @ColumnResult(name = "created_at", type = LocalDateTime.class),
                        }
                )
        }
)
@NamedNativeQuery(name = "ReservationStatus.getReservationLatestStatus", query = "SELECT trx_reservation_status.booking_no, trx_reservation_status.booking_status, trx_reservation_status.updated_at, trx_reservation_status.created_at, mst_booking_status.description, mst_booking_status.icon_url " +
        "FROM trx_reservation_status " +
        "LEFT JOIN mst_booking_status ON trx_reservation_status.booking_status = mst_booking_status.code " +
        "WHERE trx_reservation_status.booking_no = ?1 ORDER BY trx_reservation_status.id DESC LIMIT 1", resultSetMapping = "getReservationStatusDetail")
@NamedNativeQuery(name = "ReservationStatus.getReservationStatusDetail", query = "SELECT trx_reservation_status.booking_no, trx_reservation_status.booking_status, trx_reservation_status.updated_at, trx_reservation_status.created_at, mst_booking_status.description, mst_booking_status.icon_url " +
        "FROM trx_reservation_status " +
        "LEFT JOIN mst_booking_status ON trx_reservation_status.booking_status = mst_booking_status.code " +
        "WHERE trx_reservation_status.booking_no = ?1 ORDER BY trx_reservation_status.id DESC", resultSetMapping = "getReservationStatusDetail")
public class ReservationStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String bookingNo;

    @Column(nullable = false)
    private String bookingStatus;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public ReservationStatus() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}

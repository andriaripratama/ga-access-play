package id.co.garudaindonesia.gaaccess_play.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TempReserveHeaderGuest implements Serializable {

    private Long id;

    private String tempBookingNo;

    private Integer userId;

    private String userCode;

    //  Product Type allowed to be Null cause Port to Port doesn't have Product
    private String productType;

    private String serviceCode;

    private String vendorCode;

    private String status;

    private Integer confirmDateTime;

    private Integer confirmPic;

    //  This for Door to Door
    private String originLat;

    private String originLong;

    private String originAddress;

    private String destinationLat;

    private String destinationLong;

    private String destinationAddress;

    //  This for Port to Port
    private String originStation;

    private String destinationStation;

    private String flightNumber;

    private String recipientName;

    private String recipientPhone;

    private LocalDateTime created_at;

    private LocalDateTime updated_at;

    private String productName;

    private String serviceName;

    private String vendorName,vendorLogo,lastStatus;

    private String senderName,senderPhone,senderEmail,senderAddress;

    public TempReserveHeaderGuest(Long id, String tempBookingNo, Integer userId, String userCode, String productType, String serviceCode, String vendorCode, String status, Integer confirmDateTime, Integer confirmPic, String originLat, String originLong, String originAddress, String destinationLat, String destinationLong, String destinationAddress, String originStation, String destinationStation, String flightNumber, String recipientName, String recipientPhone, LocalDateTime created_at, LocalDateTime updated_at, String productName, String serviceName, String vendorName, String vendorLogo, String lastStatus, String senderName, String senderPhone, String senderEmail, String senderAddress) {
        this.id = id;
        this.tempBookingNo = tempBookingNo;
        this.userId = userId;
        this.userCode = userCode;
        this.productType = productType;
        this.serviceCode = serviceCode;
        this.vendorCode = vendorCode;
        this.status = status;
        this.confirmDateTime = confirmDateTime;
        this.confirmPic = confirmPic;
        this.originLat = originLat;
        this.originLong = originLong;
        this.originAddress = originAddress;
        this.destinationLat = destinationLat;
        this.destinationLong = destinationLong;
        this.destinationAddress = destinationAddress;
        this.originStation = originStation;
        this.destinationStation = destinationStation;
        this.flightNumber = flightNumber;
        this.recipientName = recipientName;
        this.recipientPhone = recipientPhone;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.productName = productName;
        this.serviceName = serviceName;
        this.vendorName = vendorName;
        this.vendorLogo = vendorLogo;
        this.lastStatus = lastStatus;
        this.senderName = senderName;
        this.senderPhone = senderPhone;
        this.senderEmail = senderEmail;
        this.senderAddress = senderAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTempBookingNo() {
        return tempBookingNo;
    }

    public void setTempBookingNo(String tempBookingNo) {
        this.tempBookingNo = tempBookingNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getConfirmDateTime() {
        return confirmDateTime;
    }

    public void setConfirmDateTime(Integer confirmDateTime) {
        this.confirmDateTime = confirmDateTime;
    }

    public Integer getConfirmPic() {
        return confirmPic;
    }

    public void setConfirmPic(Integer confirmPic) {
        this.confirmPic = confirmPic;
    }

    public String getOriginLat() {
        return originLat;
    }

    public void setOriginLat(String originLat) {
        this.originLat = originLat;
    }

    public String getOriginLong() {
        return originLong;
    }

    public void setOriginLong(String originLong) {
        this.originLong = originLong;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(String destinationLat) {
        this.destinationLat = destinationLat;
    }

    public String getDestinationLong() {
        return destinationLong;
    }

    public void setDestinationLong(String destinationLong) {
        this.destinationLong = destinationLong;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getOriginStation() {
        return originStation;
    }

    public void setOriginStation(String originStation) {
        this.originStation = originStation;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(String destinationStation) {
        this.destinationStation = destinationStation;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorLogo() {
        return vendorLogo;
    }

    public void setVendorLogo(String vendorLogo) {
        this.vendorLogo = vendorLogo;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }
}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.ProductByVendor;
import id.co.garudaindonesia.gaaccess_play.model.VendorProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VendorProductRepo extends JpaRepository<VendorProduct, Long> {

    VendorProduct findByVendorCodeAndProductType(String vendorCode, String productType);

//    @Query(value = "SELECT product.code, product.product_type, product.vendor_code, vendor.name, vendor.logo " +
//            "FROM mst_vendor_product product, mst_vendor vendor WHERE product.product_type = ?1 " +
//            "AND vendor.status = 'A' AND product.vendor_code = vendor.code",
//            nativeQuery = true )
    @Query(name = "VendorProduct.getAvailableVendorByProduct", nativeQuery = true)
    List<ProductByVendor> findByProductType(String productType);

}

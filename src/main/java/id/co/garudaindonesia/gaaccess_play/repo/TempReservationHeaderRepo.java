package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.ReservationHeader;
import id.co.garudaindonesia.gaaccess_play.model.TempReservationHeader;
import id.co.garudaindonesia.gaaccess_play.model.TempReserveHeaderGuest;
import id.co.garudaindonesia.gaaccess_play.model.TempReserveHeaderTrack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TempReservationHeaderRepo extends JpaRepository<TempReservationHeader, Long> {

    @Query(value = "SELECT trx_temp_reservation_header.*, mst_service.description AS service_name " +
            "FROM trx_temp_reservation_header LEFT JOIN mst_service ON trx_temp_reservation_header.service_code = mst_service.code " +
            "WHERE trx_temp_reservation_header.temp_booking_no = ?1 AND trx_temp_reservation_header.user_code = ?2", nativeQuery = true)
    TempReservationHeader findByTempBookingNoAndUserCode(String number, String userCode);

    @Procedure
    Integer confirmBooking(String bookingNo, String userCode);

    @Modifying
    @Procedure
    Integer updateApprovalStatus(String bookingNo, String userCode, String status, Integer bookingId);

    @Query(value = "select temp_booking_no from trx_temp_reservation_header where id = ?1", nativeQuery = true)
    String findTempBookingNo(Long id);

    @Query(value = "SELECT count(*) as totaldata FROM trx_temp_reservation_header WHERE temp_booking_no = ?1 AND user_code = ?2", nativeQuery = true)
    Integer checkBookingExist(String bookingNo, String userCode);

    @Query(value = "SELECT * FROM trx_temp_reservation_header ORDER BY id DESC", nativeQuery = true)
    List<TempReservationHeader> getAllReservation();

    @Query(name = "TempReservationHeader.getBookingDetail", nativeQuery = true)
    List<TempReserveHeaderTrack> getUserReservation(String userCode);

    @Query(name = "TempReservationHeader.getGuestBookingDetail", nativeQuery = true)
    List<TempReserveHeaderGuest> getGuestReservation(String userCode);

    @Transactional
    @Modifying
    @Query(value = "UPDATE trx_temp_reservation_header SET status = ?1 WHERE user_code = ?2 AND temp_booking_no = ?3", nativeQuery = true)
    void setBookingStatus(String status, String userCode, String bookingNo);


    //  For Vendor usage
    TempReservationHeader findByTempBookingNo(String bookingNo);

    //  For Partner usage (API)
    List<TempReservationHeader> findByVendorCode(String vendorCode);

    @Query(value = "SELECT * FROM trx_temp_reservation_header WHERE vendor_code = ?1 AND temp_booking_no = ?2", nativeQuery = true)
    TempReservationHeader findReservationForPartner(String vendorCode, String bookingNo);
}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ApplicationUserRepo extends JpaRepository<ApplicationUser, Long> {

//    ApplicationUser findByUsername(String username);
    ApplicationUser findByEmail(String email);
    ApplicationUser findByCellNumber(String cellNumber);
    ApplicationUser findByCode(String code);

    @Query(value = "SELECT id FROM mst_users WHERE email = ?1", nativeQuery = true)
    Integer getUserId(String email);
}

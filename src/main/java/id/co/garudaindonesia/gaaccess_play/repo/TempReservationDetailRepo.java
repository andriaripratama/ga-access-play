package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.TempReservationDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;

import java.util.List;


public interface TempReservationDetailRepo extends JpaRepository<TempReservationDetail, Long> {

    List<TempReservationDetail> findByTempBookingNo(String tempBookingNo);

    @Procedure
    void confirmBookingDetail(String bookingNo);

}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.VendorCore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorCoreRepo extends JpaRepository<VendorCore, Long> {


}

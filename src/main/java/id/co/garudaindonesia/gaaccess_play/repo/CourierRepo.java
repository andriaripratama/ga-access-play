package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.Courier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface CourierRepo extends JpaRepository<Courier, Long> {

    String FIND_PROJECTS = "SELECT id, name, address, phone_number, email_address FROM mst_courier";


    @Query(value = FIND_PROJECTS, nativeQuery = true)
    List<Courier> findCouriers();
}

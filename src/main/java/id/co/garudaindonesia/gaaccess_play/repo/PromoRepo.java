package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.Promo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PromoRepo extends JpaRepository<Promo, Long> {

//    @Query(value = "", nativeQuery = true)
//    List<Promo> allActivePromo();

    List<Promo> findByPromoType(String promoType);

}

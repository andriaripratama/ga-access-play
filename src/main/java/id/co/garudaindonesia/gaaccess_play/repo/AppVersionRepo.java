package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.AppVersion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AppVersionRepo extends JpaRepository<AppVersion, Long> {

    @Query(value = "SELECT app_version FROM app_version ORDER BY id DESC LIMIT 1", nativeQuery = true)
    String getLatestVersion(String appType);
}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.Partner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartnerRepo extends JpaRepository<Partner, Long> {

    Partner findByPartnerKey(String partnerKey);
    Partner findByVendorCode(String vendorCode);

}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.GuestReservationDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GuestReservationRepo extends JpaRepository<GuestReservationDetail, Long> {

    @Query(value = "SELECT * FROM trx_guest_reservation WHERE uuid = ?1 ORDER BY id DESC LIMIT 1", nativeQuery = true)
    GuestReservationDetail findByUuid(String UUID);
}

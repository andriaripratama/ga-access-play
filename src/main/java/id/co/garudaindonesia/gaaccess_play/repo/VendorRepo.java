package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepo extends JpaRepository<Vendor, Long> {

    Vendor findByCode(String code);

}

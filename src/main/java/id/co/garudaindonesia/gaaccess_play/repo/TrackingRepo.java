package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.Tracking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackingRepo extends JpaRepository<Tracking, Long> {

    Tracking findTopByBookingNoOrderById(String bookingNo);
}

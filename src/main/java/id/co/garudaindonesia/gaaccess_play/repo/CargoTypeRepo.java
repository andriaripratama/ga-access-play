package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.CargoType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CargoTypeRepo extends JpaRepository<CargoType, Long> {

    CargoType findByCode(String code);
}

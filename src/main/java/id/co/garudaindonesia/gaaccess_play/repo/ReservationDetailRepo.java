package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.ReservationDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface ReservationDetailRepo extends JpaRepository<ReservationDetail, Long> {

    List<ReservationDetail> findByBookingNo(String bookingNo);

    @Modifying
    @Query(value = "UPDATE trx_reservation_detail " +
            "SET length = ?3, width = ?4, height = ?5 " +
            "WHERE reserve_code = ?1 AND booking_no = ?2", nativeQuery = true)
    @Transactional
    void updateItemDetail(String reserveCode, String bookingNo, Integer length, Integer width, Integer height);
}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.Pickup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PickupRepo extends JpaRepository<Pickup, Long> {

    List<Pickup> findByVendorCode(String vendorCode);

    Pickup findByVendorCodeAndBookingNoAndDriverId(String vendorCode, String bookingNo, String driverId);
}

package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.BookingStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingStatusRepo extends JpaRepository<BookingStatus, Long> {

    BookingStatus findByCode(String code);
}

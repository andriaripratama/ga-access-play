package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.NearbyVendor;
import id.co.garudaindonesia.gaaccess_play.model.VendorBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VendorBranchRepo extends JpaRepository<VendorBranch, Long> {

    @Query(name = "VendorBranch.getNearbyVendor", nativeQuery = true)
    List<NearbyVendor> findNearbyBranch(Float latitude, Float longitude, Number distance);

}

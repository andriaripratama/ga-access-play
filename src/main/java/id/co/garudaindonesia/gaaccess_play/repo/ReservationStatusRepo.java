package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.ReservationStatus;
import id.co.garudaindonesia.gaaccess_play.model.ReservationStatusDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ReservationStatusRepo extends JpaRepository<ReservationStatus, Long> {

    List<ReservationStatus> findByBookingNo(String bookingNo);

    @Query(name = "ReservationStatus.getReservationLatestStatus", nativeQuery = true)
    ReservationStatusDetail getLatestStatus(String bookingNo);

    @Query(name = "ReservationStatus.getReservationStatusDetail", nativeQuery = true)
    List<ReservationStatusDetail> getDetailStatus(String bookingNo);

    @Transactional
    @Modifying
    @Procedure
    Integer updateReservationStatus(String statusCode, String bookingNo, String userCode);

    @Transactional
    @Modifying
    @Procedure
    Integer updateShipmentStatus(String statusCode, String bookingNo);
}

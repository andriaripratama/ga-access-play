package id.co.garudaindonesia.gaaccess_play.repo;

import id.co.garudaindonesia.gaaccess_play.model.ReservationHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface ReservationHeaderRepo extends JpaRepository<ReservationHeader, Long> {

    ReservationHeader findByBookingNo(String bookingNo);

    ReservationHeader findByBookingNoAndUserCode(String bookingNo, String userCode);

}
